module ci.tno.nl/gitlab/ppa-project/pkg/secret

go 1.15

require github.com/stretchr/testify v1.7.0

replace ci.tno.nl/gitlab/ppa-project/pkg/paillier => ci.tno.nl/gitlab/ppa-project/pkg/paillier.git v1.0.1
